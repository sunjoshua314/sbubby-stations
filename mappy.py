import networkx as nx
import yaml
from PIL import Image, ImageDraw, ImageFont

import graphy


"""
Function to generate intermediate "fake stops" such that all stops are at a 90 or 45 degree
angle from one another.
"""


def straighten_coordinates():

    # Counter used to create unique ids for fake stops
    id = 0

    # Save state of current graph in an "old" graph
    old = nx.Graph()
    old.update(g)

    # Iterate over each connection, generating an intermdediate connection
    for edge in old.edges:

        # Unpack coordinates of the two stops (sort edge for consistent unpacking)
        c1, c2 = (g.nodes[node]["coordinates"] for node in sorted(edge))

        # Distance between the two coordinates, in x and y
        x_delta = c2["x"] - c1["x"]
        z_delta = c2["z"] - c1["z"]

        # Positive value for distance
        x_magnitude = abs(x_delta)
        z_magnitude = abs(z_delta)

        # 1 or -1 to indicate direction
        x_sign = 1 if x_delta >= 0 else -1
        z_sign = 1 if z_delta >= 0 else -1

        # Line is split into either horizontal line and 45 degree line,
        # or vertical line and 45 degree line,
        # depending on slope of the line.
        #
        # For example, if the line is more horizontal than vertical, separate it into a horizontal
        # line and a 45 degree line
        if x_magnitude > z_magnitude:
            x = c1["x"] + x_sign * (x_magnitude - z_magnitude)
            z = c1["z"]
        else:
            x = c1["x"]
            z = c1["z"] + z_sign * (z_magnitude - x_magnitude)

        # Create the two new lines using the calculated coordinates, and remove the old line
        g.add_node(id, coordinates={"x": x, "y": 0, "z": z}, line=get_line(edge))
        g.add_edge(edge[0], id)
        g.add_edge(id, edge[1])
        g.remove_edge(edge[0], edge[1])

        # Use a new id for the next fake node to add
        id += 1


"""
PIL's ImageDraw module orients images with the (0,0) pixel in the top-left corner.

Our map follows standard Cartesian coordinates, so we need to re-orient the stops on PIL"s
coordinate system. Additionally, this function will format each dimension to include factors such
as spacing, buffer on the edges of the image, etc.

RETURNS: autoscaled dimensions to use for the image
"""


def format():

    min_x = 10000000
    min_z = 10000000
    max_x = -10000000
    max_z = -10000000

    for coordinate in coordinates.values():
        min_x = min(coordinate["x"], min_x)
        min_z = min(coordinate["z"], min_z)
        max_x = max(coordinate["x"], max_x)
        max_z = max(coordinate["z"], max_z)

    for coordinate in coordinates.values():
        coordinate["x"] = (coordinate["x"] - min_x) * SPACING + BUFFER
        coordinate["z"] = (coordinate["z"] - min_z) * SPACING + BUFFER

    dimensions = (
        (max_x - min_x) * SPACING + 2 * BUFFER,
        (max_z - min_z) * SPACING + 2 * BUFFER,
    )

    return dimensions


"""
Draw an ellipse with "coordinate" being the top-left corner of the bounding box
"""


def draw_ellipse(coordinate, stop_type="station"):

    if stop_type == "station":
        radius = STATION_SIZE / 2
    elif stop_type == "switch":
        radius = SWITCH_SIZE / 2

    # Define top left and bottom right corner of ellipse's bounding box
    topleft = (coordinate["x"] - radius, coordinate["z"] - radius)
    bottomright = (coordinate["x"] + radius, coordinate["z"] + radius)

    # Draw a station
    draw.ellipse(
        [topleft, bottomright], fill="blue", outline="black", width=STATION_BORDER_WIDTH
    )


"""
Draw a diamond with the "coordinate" being the top-left corner of the bounding box
"""


def draw_diamond(coordinate, stop_type="switch"):

    if stop_type == "station":
        radius = STATION_SIZE / 2
    elif stop_type == "switch":
        radius = SWITCH_SIZE / 2

    # Define four points of the diamond
    left = (coordinate["x"] - radius, coordinate["z"])
    top = (coordinate["x"], coordinate["z"] - radius)
    right = (coordinate["x"] + radius, coordinate["z"])
    bottom = (coordinate["x"], coordinate["z"] + radius)

    # Draw a station
    draw.polygon([left, top, right, bottom], fill="yellow", outline="black")


"""
Draw a connection between two stops
"""


def draw_line(edge):
    node1, node2 = (g.nodes[node]["coordinates"] for node in edge)
    coordinates1 = (node1["x"], node1["z"])
    coordinates2 = (node2["x"], node2["z"])
    color = line_colors.get(get_line(edge), "black")
    draw.line([coordinates1, coordinates2], fill=color, width=LINE_WIDTH)


"""
Draws a label with the station name next to the station on the map
"""


def label(name, coordinate):
    location = tuple(int(coordinate[axis] + TEXT_OFFSET[axis]) for axis in ["x", "z"])
    customfont = ImageFont.truetype(font="Pixel_Tandy.otf", size=TEXT_SIZE)
    txt = Image.new("RGBA", customfont.getsize(name), color="rgba(200,255,255,100)")
    txt_draw = ImageDraw.Draw(txt)
    txt_draw.text((0, -2), name, fill="black", font=customfont)
    im.paste(txt, location, txt)


"""
Determines what train line the edge is on.

Connections can either be between two stations, a station and a switch, or two switches. In any
case, the connection will always be on exactly one line; adjacent stations should always be on the
same line. Therefore, we have the following cases:

2 stations: just return one of the station's lines, since they are the same
1 station, 1 switch: find the station and return that line
2 switches: neither has a line, return None
"""


def get_line(edge):
    line1 = g.nodes[edge[0]].get("line", None)
    line2 = g.nodes[edge[1]].get("line", None)

    if line1:
        return line1
    elif line2:
        return line2


"""
CONSTANTS
"""
# Scaling ratio for map, controls spacing between stops
SPACING = 1

# Number of pixels between edge of image and first stop in x and y axes
BUFFER = 100

# Diameter of each station, in pixels
STATION_SIZE = 16

# Diameter of each switch, in pixels
SWITCH_SIZE = 12

# Width of colored outline around stations
STATION_BORDER_WIDTH = 2

# Width of lines between stops
LINE_WIDTH = 8

# Location of station name text relative to station
TEXT_OFFSET = {"x": STATION_SIZE / 2, "z": STATION_SIZE / 2 - 4}

# Size of station name text
TEXT_SIZE = 10

# Unpack the map data
g, stations, switches = graphy.get_graph()
coordinates = nx.get_node_attributes(g, "coordinates")
station_coordinates = nx.get_node_attributes(stations, "coordinates")
switch_coordinates = nx.get_node_attributes(switches, "coordinates")
station_lines = nx.get_node_attributes(stations, "line")

# Re-orient stops on new axes, saving the dimensions for the image generation
dimensions = format()

# Restrict lines to 45 and 90 degree angles
straighten_coordinates()
coordinates = nx.get_node_attributes(g, "coordinates")

# Create image object
im = Image.new("RGB", dimensions, color="white")

# Start drawing stuff in the image
draw = ImageDraw.Draw(im)

# Assign a color to each line
line_colors = {}
lines = graphy.get_lines()
for (name, line) in lines.items():
    line_colors[name] = line["color"]

# Draw each line
for edge in g.edges:
    draw_line(edge)

# Draw stations as ellipses (default) and label them
for stop, coordinate in station_coordinates.items():
    draw_ellipse(coordinate)
    label(stop, coordinate)

# Draw switches as diamonds (default)
for stop, coordinate in switch_coordinates.items():
    draw_diamond(coordinate)

# Save image to file
im.save("map.png")
