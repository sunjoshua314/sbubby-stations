import networkx as nx
import yaml

"""
In this context, a stop is either a station or a switch.
Given properly formatted stop dictionaries, return dictionaries mapping...

stops -> coordinates

AND

stops -> connections

...returning both dictionaries.

"""


def _process(data):
    coordinates = {}
    connections = {}
    lines = {}

    for stop, stop_data in data.items():
        coordinates[stop] = stop_data.get("coordinates")
        connections[stop] = stop_data.get("connections")
        lines[stop] = stop_data.get("line", None)

    return (coordinates, connections, lines)


def get_graph():

    # Load YAML from files
    data = yaml.full_load(open("inputs.yml", "r"))

    # Process switches, stations into respective coordinates and connections dictionaries
    station_coordinates, station_connections, station_lines = _process(data["stations"])
    switch_coordinates, switch_connections, switch_lines = _process(data["switches"])

    # Build stations graph from station dictionary, adding metadata tags
    stations = nx.from_dict_of_lists(station_connections)
    nx.set_node_attributes(stations, station_coordinates, "coordinates")
    nx.set_node_attributes(stations, station_lines, "line")

    # Build switches graph from switch dictionary, adding metadata tags
    switches = nx.from_dict_of_dicts(switch_connections)
    nx.set_node_attributes(switches, switch_coordinates, "coordinates")

    # Combine graphs
    g = nx.Graph()
    g.update(stations)
    g.update(switches)

    return (g, stations, switches)


def get_lines():
    data = yaml.full_load(open("inputs.yml", "r"))
    return data["lines"]
